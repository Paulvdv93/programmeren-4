<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inschrijven Concert</title>
</head>
<body>
    <h1>Bob Dylan</h1>
    <form action="behandelreservatie.php" method="post">
        <div id="voornaam">
        <label for="voornaam">Voornaam: </label>
        <input type="text" name="voornaam"/></br>
        <span> <?php echo $voornaamErr;?></span></br>
        </div>
        <div id="naam">
        <label for="naam">Naam: </label>
        <input type="text" name="naam"/></br>
        <span> <?php echo $naamErr;?></span></br>
        </div>
        <div id="plaats">
        <label for="plaats">Plaats: </label>
        <select name="stages" id="stages">
            <option value="0">Stage 1</option>
            <option value="1">Stage 2</option>
            <option value="2">Stage 3</option>
            <option value="3">Stage 4</option>
        </select>
        <span> <?php echo $stageErr;?></span></br>
        <label for="tribunenr">Tribunenummer: </label>
        <input type="number" name="tribunenr" min="1"/>
        <span> <?php echo $tribuneErr;?></span></br>
        <label for="plaatsnr">Plaatsnummer: </label>
        <input type="number" name="plaatsnr" min="1"/></br>
        <span> <?php echo $plaatsErr;?></span></br>
        </div>
        <div id="aantalpers">
        <label for="aantalpers">Aantal Personen: </label>
        <input type="number" name="aantalpers" min="1"/></br>
        <span> <?php echo $aantalpersErr;?></span></br>
        </div>
        <div id="gebdatum">
        <label for="gebdatum">Geboortedatum: </label>
        <input type="date" name="gebdatum"/></br>
        <span> <?php echo $gebdatumErr;?></span></br>
        </div>
        <div id="rekeningnummer">
        <label for="rekeningnummer">Rekeningnummer: </label>
        <input type="text" name="rekeningnummer" placeholder="BE12 3456 7890 1234"/></br>
        <span> <?php echo $reknrErr;?></span></br>
        </div>
        <div id="emailadres">
        <label for="emailadres">E-mailadres: </label>
        <input type="email" name="emailadres" placeholder="random@random.random"/></br>
        <span> <?php echo $emailErr;?></span></br>
        </div>
        <input type="submit" value="Verzenden"/>
    </form>
</body>
</html>
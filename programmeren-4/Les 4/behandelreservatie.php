<?php
    $voornaamErr = $naamErr = $stageErr = $tribuneErr = $plaatsErr = $aantalpersErr = $reknrErr = $emailErr = "";
    $voornaam = $naam = $stagenr = $tribunenr = $plaatsnr = $aantalpers = $gebdatum
        = $reknr = $email = "";
        
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        //Voornaam Validatie
        if(empty($_POST["voornaam"])){
            $voornaamErr = "Voornaam is vereist!";
        }
        else {
            $voornaam = $_POST["voornaam"];
            if(!preg_match("/^[a-zA-Z]+$/", $voornaam)){
                $voornaamErr = "Geef een geldige voornaam op!";
            }
        }
        //Naam Validatie
        if(empty($_POST["naam"])){
            $naamErr = "Achternaam is vereist!";
        }
        else {
            $naam = ($_POST["naam"]);
            if(!preg_match("/^[a-zA-Z]+$/", $naam)){
                $naamErr = "Geef een geldige achternaam op!";
            }
        }
        //Aantal Personen Validatie
        if(empty($_POST["aantalpers"])){
            $aantalpersErr = "Aantal personen ingeven is vereist!";
        }
        else {
            $aantalpers = $_POST["aantalpers"];
            if($aantalpers <= 0){
                $aantalpersErr = "Mag alleen een natuurlijk getal zijn!";
        }
        //GeboorteDatum Validatie
        $gebdatum = $_POST['gebdatum'];
        //Rekeningnummer Validatie
        if (empty($_POST["rekeningnummer"])) {
            $reknrErr = "Rekeningnummer is vereist!";
        }
        else {
            $reknr = $_POST["rekeningnummer"];
            if(!preg_match("/^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}$/", $reknr)){
                $reknrErr = "Rekeningnummer is niet geldig!";
            }
        }
        //E-mailadres Validatie
        if (empty($_POST["emailadres"])) {
            $emailErr = "E-mailadres is vereist!";
        }
        else {
            $email = $_POST["emailadres"];
            if(!preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/", $email)){
                $emailErr = "E-mailadres is niet geldig!";
            }
        }
        //Stagenummers
        if(isset($_POST['stages'])){
            $stagenr = $_POST['stages'];
            switch ($stagenr) {
                case 0 :
                        $stagenr = 'Stage 1';
                break;
                case 1 :
                        $stagenr = 'Stage 2';
                break;        
                case 2 :
                        $stagenr = 'Stage 3';
                break;
                case 3 :
                        $stagenr = 'Stage 4';
                default:
                    $stagenr = 'Geen gekozen';
                break;
            }
            if($stagenr == 'Geen gekozen'){
                $stageErr = "Gelieve een stage te selecteren!";
            }
        }
        if(isset($_POST["tribunenr"])){
            $tribunenr = $_POST["tribunenr"];
            if($tribunenr <= 0 && $tribunenr > 20){
                $tribuneErr = "Geef een geldig tribunenummer in!";
            }
        }
        if(isset($_POST["plaatsnr"])){
            $plaatsnr = $_POST["plaatsnr"];
            if($plaatsnr <= 0 && $plaatsnr > 50){
                $plaatserr = "Geef een geldig plaatsnummer in!";
            }
        }
    }
    }
?>
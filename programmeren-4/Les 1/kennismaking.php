<!doctype html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <title>Kennismaking met PHP</title>
</head>

<body>
    <h1>Kennismaking</h1>
    <h2>PHP</h2>
    <form action="verwerken.php" method="post">
        <div>
            <label for="firstname">Voornaam: </label>
            <input type="text" name="firstname" id="firstname" />
        </div>
        <div>
            <label for="lastname">Achternaam: </label>
            <input type="text" name="lastname" id="lastname" />
        </div>
        <input type="submit" value="Verzenden"/>
    </form>
</body>

</html>

<?php
$file = file_get_contents("Data/Postcodes.csv");
$file = utf8_encode($file);

$postcodes = explode("\n", $file);
$array = array();

foreach ($postcodes as $postcode) {
    $temp = explode("|", $postcode);
    array_push($array, $temp);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lezen Van Postcodes</title>
</head>
<body>
    <table>
        <tr>
            <th>
                Postcode
            </th>
            <th>
                Stad in Nederlands
            </th>
            <th>
                District in Nederlands
            </th>
            <th>
                Stad in Frans
            </th>
            <th>
                District in Frans
            </th>
        </tr>
        <?php
            foreach ($array as $postcodeItem) { ?>
                <tr>
                    <td> <?php echo $postcodeItem[0];?></td>
                    <td> <?php echo $postcodeItem[1];?></td>
                    <td> <?php echo $postcodeItem[2];?></td>
                    <td> <?php echo $postcodeItem[3];?></td>
                    <td> <?php echo $postcodeItem[4];?></td>
                </tr>
            <?php }
        ?>
    </table>
</body>
</html>
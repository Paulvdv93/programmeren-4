<?php 

session_start();

$products = array("Gitaar", "Blokfluit", "Drumstel", "Akoestische Gitaar", "Bassgitaar", "Elektrisch Drumstel", "Trompet", "Viool", "Piano", "Klarinet");
$HoeveelheidPrijs = array("200", "50", "900", "150", "500", "1200", "700", "800", "2000", "300");

 if ( !isset($_SESSION["total"]) ) {

   $_SESSION["total"] = 0;
   for ($i=0; $i< count($products); $i++) {
    $_SESSION["ProductHoeveelheidPrijs"][$i] = 0;
   $_SESSION["HoeveelheidPrijs"][$i] = 0;
  }
 }

 if ( isset($_GET['reset']) )
 {
 if ($_GET["reset"] == 'true')
   {
   unset($_SESSION["ProductHoeveelheidPrijs"]);
   unset($_SESSION["HoeveelheidPrijs"]);
   unset($_SESSION["total"]);
   unset($_SESSION["mand"]);
   }
 }

 if ( isset($_GET["add"]) )
   {
   $i = $_GET["add"];
   $ProductHoeveelheidPrijs = $_SESSION["ProductHoeveelheidPrijs"][$i] + 1;
   $_SESSION["HoeveelheidPrijs"][$i] = $HoeveelheidPrijs[$i] * $ProductHoeveelheidPrijs;
   $_SESSION["mand"][$i] = $i;
   $_SESSION["ProductHoeveelheidPrijs"][$i] = $ProductHoeveelheidPrijs;
 }

  if ( isset($_GET["delete"]) )
   {
   $i = $_GET["delete"];
   $ProductHoeveelheidPrijs = $_SESSION["ProductHoeveelheidPrijs"][$i];
   $ProductHoeveelheidPrijs--;
   $_SESSION["ProductHoeveelheidPrijs"][$i] = $ProductHoeveelheidPrijs;
   if ($ProductHoeveelheidPrijs == 0) {
    $_SESSION["HoeveelheidPrijs"][$i] = 0;
    unset($_SESSION["mand"][$i]);
  }
 else
  {
   $_SESSION["HoeveelheidPrijs"][$i] = $HoeveelheidPrijs[$i] * $ProductHoeveelheidPrijs;
  }
 }
 ?>

 
 <!doctype html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <title>Document</title>
 </head>
 <body>
      <h2>List of All Products</h2>
      <background: #90a090;>
 <table>
   <tr>
   <th>Product</th>
   <th width="10px">&nbsp;</th>
   <th>Amount</th>
   <th width="10px">&nbsp;</th>
   <th>Action</th>
   </tr>
 <?php
 for ($i=0; $i< count($products); $i++) 
 {
   ?>
   <tr>
   <td><?php echo($products[$i]); ?></td>
   <td width="10px">&nbsp;</td>
   <td><?php echo($HoeveelheidPrijs[$i]); ?></td>
   <td width="10px">&nbsp;</td>
   <td><a href="?add=<?php echo($i); ?>">Add</a></td>
   </tr>
   <?php
 }
 ?>
     <tr>
     <td colspan="5"></td>
     </tr>
     <tr>
     <td colspan="5"><a href="?reset=true">Clear Mand</a></td>
     </tr>
     </table>
    <?php
     if ( isset($_SESSION["mand"]) ) 
     {
     ?>
     <br/><br/><br/>
     <h2>mand</h2>
     <table>
     <tr>
     <th>Product</th>
     <th width="10px">&nbsp;</th>
     <th>Hoeveelheid</th>
     <th width="10px">&nbsp;</th>
     <th>Prijs</th>
     <th width="10px">&nbsp;</th>
     <th>Actie</th>
     </tr>
 <?php
     $total = 0;
     foreach ( $_SESSION["mand"] as $i ) 
     {
     ?>
     <tr>
     <td><?php echo( $products[$_SESSION["mand"][$i]] ); ?></td>
     <td width="10px">&nbsp;</td>
     <td><?php echo( $_SESSION["ProductHoeveelheidPrijs"][$i] ); ?></td>
     <td width="10px">&nbsp;</td>
     <td><?php echo( $_SESSION["HoeveelheidPrijs"][$i] ); ?></td>
     <td width="10px">&nbsp;</td>
     <td><a href="?delete=<?php echo($i); ?>">Delete</a></td>
     </tr>
    <?php
    $total = $total + $_SESSION["HoeveelheidPrijs"][$i];
     }
     $_SESSION["total"] = $total;
     ?>
     <p>
     <tr>
     <p>
     <td colspan="7">Total : <?php echo($total); ?></td>
     </tr>
     </table>
 <?php
     }
 ?>
     
     
 </body>
 </html>
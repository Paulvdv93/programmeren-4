<?php
/*if(isset($_POST['product'])){
    if(isset($_COOKIE['cart'])){
        $cart = unserialize($_COOKIE['cart']);
    }
    $cart[] = $_POST['product'];
    setcookie('cart', serialize ($cart), time()+60+60+24+15);
}*/

$winkelkar = array();
 if (isset($_POST['artikelen'])) {
                if(isset($_COOKIE['winkelkarbijhouden'])){
                        $artikels = unserialize($_COOKIE['winkelkarbijhouden']);
                }
                $artikels[] = $_POST['artikelen'];
                setcookie("winkelkarbijhouden", serialize($artikels), time() + (86400 * 7), "/");
            }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Werken met Cookies</title>
</head>
<body>
    <h1>Webwinkel van Polleke</h1>
    
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
        <div>
        <label for="artikelen">Artikelen: </label></br>
        <select name="artikelen" id="artikelen">
            <option value="Coca-Cola">Coca-Cola</option>
            <option value="Fanta">Fanta</option>
            <option value="Sprite">Sprite</option>
            <option value="Aquarius">Aquarius</option>
            <option value="Lipton Ice-Tea">Lipton Ice-Tea</option>
            <option value="Mountain Dew">Mountain Dew</option>
            <option value="Red Bull">Red Bull</option>
            <option value="Monster">Monster</option>
            <option value="AA Drink">AA Drink</option>
            <option value="Nalu">Nalu</option>
        </select>
        </div>
        <button type="submit">Aankopen</button>
    </form>
    
    
    <div id="winkelkar">
        <h2>WinkelKar</h2>
        <?php
        if(isset($_COOKIE['winkelkarbijhouden'])){
        $artikels = unserialize($_COOKIE['winkelkarbijhouden']);
        foreach ($artikels as $item) {
            echo $item . '<br>';
        }
       }else{
            echo 'Uw winkelwagen is nog leeg!';
        }
        ?>
    </div>
</body>
</html>
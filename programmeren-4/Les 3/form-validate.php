<?php
        $productErr = $priceErr = $genderText = "";
        $product = $price = $gender  = "";
        
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            if (empty($_POST["product"])){
                $productErr = "Product is required";
            }
            else {
                $product = test_input($_POST["product"]);
                strip_tags($product);
            }
            if(empty($_POST["$price"])){
                $priceErr = "Price is required";
            } else {
                $price = test_input($_POST["price"]);
                if (!preg_match("^[0-9]+(\.[0-9]{1,2})?$", $price)){
                $priceErr = "Invalid Prijs";
                }
            }
            if (isset($_POST['gender'])) {
                $gender = $_POST['gender'];
                switch ($gender) {
                    case 0 :
                        $genderText = 'Man';
                    break;
                    case 1 :
                        $genderText = 'Vrouw';
                    break;        
                    case 2 :
                        $genderText = 'Anders';
                    break;
                    default :
                        $genderText = 'onbekend';
                                    }
            }
            if (isset($_POST['category'])) {
                $category = $_POST['category'];
                switch ($category) {
                    case 0 :
                    $categoryText = 'Huishoudgerief';
                    break;
                    case 1 :
                    $categoryText = 'Electronica';
                    break;        
                    case 2 :
                    $categoryText = 'Snoep';
                    break;
                    case 3 :
                    $categoryText = 'Boeken';
                    break;
                    default :
                    $categoryText = 'Onbekend';
        }
                
            }
        }
        ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulier validatie</title>
</head>
<body>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
            <label for="product">Product: </label>
            <input id="product" name="product" type="text">
            <span> * <?php echo $productErr;?></span></br>
            <label for="price">Price: </label>
            <input id="price" name="price" type="text">
            <span> * <?php echo $priceErr;?></span></br>
            <div>
            <label for="man">Man</label>
            <input type="radio" name="gender" value=0 id="man"/>
            <label for="woman">Vrouw</label>
            <input type="radio" name="gender" value=1/ id="woman">
            <label for="other">Anders</label>
            <input type="radio" name="gender" value=1/ id="other">
            </div>
            <div>
                <label for="category">Categorie</label>
                <select name="category" id="category">
                    <option value="0">Huishoudgerief</option>
                    <option value="1">Electronica</option>
                    <option value="2">Snoep</option>
                    <option value="3">Boeken</option>
                </select>
           </div>
            <button type="submit">Verzenden</button>
        </form>
        
        <p> <?php echo "Je bent een $genderText";?></p>
        <p><?php echo "Je bent een {$categoryText}";?></p>
</body>
</html>
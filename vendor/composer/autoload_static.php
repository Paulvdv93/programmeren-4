<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita89e9711f358519008ad5d5ae16141e9
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'ModernWays\\Mvc\\' => 15,
            'ModernWays\\Dialog\\' => 18,
            'ModernWays\\AnOrmApart\\' => 22,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'ModernWays\\Mvc\\' => 
        array (
            0 => __DIR__ . '/..' . '/modernways/mvc/src',
        ),
        'ModernWays\\Dialog\\' => 
        array (
            0 => __DIR__ . '/..' . '/modernways/dialog/src',
        ),
        'ModernWays\\AnOrmApart\\' => 
        array (
            0 => __DIR__ . '/..' . '/modernways/anormapart/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita89e9711f358519008ad5d5ae16141e9::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita89e9711f358519008ad5d5ae16141e9::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
